from .models import User, Book
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render
from .form import BookForm
from django.core.exceptions import ValidationError
from passlib.hash import sha256_crypt


def index(request):
    data = {"users": User.objects.all()}
    return render(request, 'index.html', context=data)


def create(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        password = request.POST.get('password')
        conf_pass = request.POST.get('conf_pass')
        email = request.POST.get('email')
        if password == conf_pass:
            if User.objects.filter(email=email):
                raise ValidationError('This user already exist')
            else:
                user = User()
                user.name = name
                user.password = sha256_crypt.encrypt(password)
                user.email = email
                user.save()
        else:
            raise ValidationError('Passwords do not match')
    return HttpResponseRedirect("/")


def login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        user = User.objects.get(email=email)
        if user:
            if sha256_crypt.verify(str(request.POST.get('password_au')), user.password):
                form = BookForm()
                data = {"books": user.book_set.all(), "user": user, "form": form}
                return render(request, 'room.html', context=data)
            else:
                raise ValidationError('Wrong password')
        else:
            raise ValidationError('This user not exist')
    return HttpResponseRedirect("/")


def add_book(request, userid):
    if request.method == 'POST':
        user = User.objects.get(id=userid)
        name = request.POST.get('name')
        author = request.POST.get('author')
        start_day = request.POST.get('start_day')
        end_day = request.POST.get('end_day')
        notes = request.POST.get('notes')
        user.book_set.create(name=name, author=author, start_day=start_day, end_day=end_day, notes=notes)
        form = BookForm()
        data = {"books": user.book_set.all(), "user": user, "form": form}
        return render(request, 'room.html', context=data)


def delete_book(request, userid, bookid):
    book = Book.objects.get(id=bookid)
    user = User.objects.get(id=userid)
    book.delete()
    form = BookForm()
    data = {"books": user.book_set.all(), "user": user, "form": form}
    return render(request, 'room.html', context=data)


def edit_book(request, bookid):
    try:
        book = Book.objects.get(id=bookid)

        if request.method == "POST":
            book.name = request.POST.get('name')
            book.author = request.POST.get('author')
            book.start_day = request.POST.get('start_day')
            book.end_day = request.POST.get('end_day')
            book.notes = request.POST.get('notes')
            book.save()

            form = BookForm()
            user = book.user
            data = {"books": user.book_set.all(), "user": user, "form": form}
            return render(request, 'room.html', context=data)
        else:
            form = BookForm()
            data = {"book": book, "form": form}
            return render(request, "edit.html", context=data)
    except Book.DoesNotExist:
        raise ValidationError('This book not exist')