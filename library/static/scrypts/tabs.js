function openTab(evt, action) {
    var i, tabcontent, tablinks;

    tabcontent = $('div.tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = $('.tablinks');
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(action).style.display = "block";
    evt.currentTarget.className += " active";
}