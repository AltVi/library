# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.db import models

class User(models.Model):
    name = models.CharField(max_length=20)
    password = models.TextField(help_text=u'Password')
    email = models.EmailField(unique=True)


class Book(models.Model):
    name = models.CharField(u'Name of the event', help_text=u'Name of the book', max_length=30)
    author = models.CharField(u'Name of the event', help_text=u'Author of the book', max_length=30)
    start_day = models.DateField(u'Day of the event', help_text=u'Book taking day')
    end_day = models.DateField(u'Final time', help_text=u'Book return day')
    notes = models.TextField(u'Textual Notes', help_text=u'Textual Notes', blank=True, null=True)
    user = models.ForeignKey(User, on_delete = models.CASCADE)