from django.test import TestCase, Client
from django.urls import reverse
from .form import BookForm
from .models import Book, User
from datetime import datetime

class BaseModelTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super(BaseModelTestCase, cls).setUpClass()
        cls.user = User(name='user', email='user@gmail.com', password='123')
        cls.user.save()
        cls.user2 = User(name='test', email='test@gmail.com', password='123')
        cls.user2.save()
        cls.first_book = Book(name="short_history_of_time", author="first",
        	                  user=cls.user, start_day=datetime.now(), end_day=datetime.now())
        cls.first_book.save()
        cls.second_book = Book(name="long_history_of_time", author="second",
        	                   user=cls.user, start_day=datetime.now(), end_day=datetime.now())
        cls.second_book.save()


class UserModelTestCase(BaseModelTestCase):
    def test_created_properly(self):
        self.assertEqual(self.user.name, 'user')
        self.assertTrue(self.first_book in self.user.book_set.all())

    def test_delete_user(self):
        self.user2.delete()
        self.assertEqual(1, len(User.objects.all()))


class BookModelTestCase(BaseModelTestCase):

    def test_created_properly(self):
        self.assertEqual(2, len(Book.objects.all()))
        self.assertEqual(1, len(Book.objects.filter(name__startswith='long')))

    def test_delete_book(self):
        self.first_book.delete()
        self.assertEqual(1, len(self.user.book_set.all()))


class User_Form_Test(TestCase):
    # Valid Form Data
    def test_BookForm_valid(self):
        form = BookForm(data={'name': "ABC", 'author': "ABC", "start_day": datetime.now(),
                              "end_day": datetime.now()})
        self.assertTrue(form.is_valid())

    # Invalid Form Data
    def test_BookForm_invalid(self):
        form = BookForm(data={'name': "", 'author': ""})
        self.assertFalse(form.is_valid())


class TestViews(TestCase):

    def test_index(self):
        self.Client = Client()
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')