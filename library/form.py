from django import forms
from datetime import datetime, timedelta


START_DAY = datetime.now()
END_DAY = START_DAY + timedelta(days=1)


class BookForm(forms.Form):
    name = forms.CharField(required=True)
    author = forms.CharField(required=True)
    start_day = forms.DateField(initial=START_DAY)
    end_day = forms.DateField(initial=END_DAY)
    notes = forms.CharField(required=False)