This is a simple version(pre-alfa) of the library.
It supports user registration / authorization and adding / changing / deleting books  

for loading fixtures use  
	*python manage.py loaddata fixtures/db.json  
Passwords are "salted" ((login -> password) 123 -> 123, alt -> 1)  
To run the tests, enter  
	*python manage.py test  
To run application:  
	*python manage.py runserver  