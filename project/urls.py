from django.contrib import admin
from django.urls import path
from library import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('create/', views.create),
    path('login/', views.login),
    path('book/<int:userid>', views.add_book),
    path('delete/<int:userid>/<int:bookid>', views.delete_book),
    path('edit/<int:bookid>', views.edit_book),
    path('', views.index),
]